#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
#include <stdbool.h>

int process_execute (const char *file_name);
int process_wait (int thread_id);
void process_exit ();
void process_activate (void);

struct process_meta{
	//process information
	int pid;
	struct semaphore *load_sema;
	struct semaphore *wait_sema;
	struct list_elem element; // used for the parent to store all his child processes
	bool is_alive;
	bool parrent_alive;
	int exit_status;
};


#endif /* userprog/process.h */
