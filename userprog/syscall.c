#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "list.h"
#include "threads/synch.h"
#include "filesys/off_t.h"

// lock used for files sys calls(synchronization)
struct lock file_lock;

//system calls
static void syscall_handler(struct intr_frame *);
static void sys_halt(void);
static void sys_exit(int status);
static int sys_exec(const char *file);
static int sys_wait(int pid);
static bool sys_create(const char *file, unsigned initial_size);
static bool sys_remove(const char *file);
static int sys_open(const char *file);
static int sys_filesize(int fd);
static int sys_read(int fd, void *buffer, unsigned size);
static int sys_write(int fd, const void *buffer, unsigned size);
static void sys_seek(int fd, unsigned position);
static unsigned sys_tell(int fd);
static void sys_close(int fd);

static int get_user(const uint8_t *uaddr);
static bool put_user(uint8_t *uaddr, uint8_t byte);

//System call function handler type
typedef int (*call_handler)(uint32_t, uint32_t, uint32_t);

static call_handler handler_map[32];

void syscall_init(void) {
	intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");

	handler_map[SYS_HALT] = (call_handler) sys_halt;
	handler_map[SYS_EXIT] = (call_handler) sys_exit;
	handler_map[SYS_EXEC] = (call_handler) sys_exec;
	handler_map[SYS_WAIT] = (call_handler) sys_wait;
	handler_map[SYS_CREATE] = (call_handler) sys_create;
	handler_map[SYS_REMOVE] = (call_handler) sys_remove;
	handler_map[SYS_OPEN] = (call_handler) sys_open;
	handler_map[SYS_FILESIZE] = (call_handler) sys_filesize;
	handler_map[SYS_READ] = (call_handler) sys_read;
	handler_map[SYS_WRITE] = (call_handler) sys_write;
	handler_map[SYS_SEEK] = (call_handler) sys_seek;
	handler_map[SYS_TELL] = (call_handler) sys_tell;
	handler_map[SYS_CLOSE] = (call_handler) sys_close;

	lock_init(&file_lock);
}

static void syscall_handler(struct intr_frame *f) {
	call_handler sys_call;
	int *param = f->esp;
	int ret_syscall;

	if (is_user_vaddr(param) == -1) {
		sys_exit(ERROR_STATUS);
	}

	if (!is_user_vaddr(param + 1) || !is_user_vaddr(param + 2)
			|| !is_user_vaddr(param + 3)) {
		sys_exit(ERROR_STATUS);
	}

	if (*param < SYS_HALT || *param > SYS_INUMBER) {
		sys_exit(ERROR_STATUS);
	} //check whether it is an accepted system call number or not

	sys_call = handler_map[*param];
	ret_syscall = sys_call(*(param + 1), *(param + 2), *(param + 3));
	f->eax = ret_syscall;
	return;
}

static void sys_halt(void) {
	shutdown_power_off();
}

static void sys_exit(int status) {
	//free resources, either here and/or in process_exit
	struct thread *cur = thread_current();

	if (cur->is_user) {
		cur->p_info->exit_status = status;
	}

	thread_exit();
}

static int sys_exec(const char *file) {
	if (-1 == get_user((const uint8_t *) file)) {
		sys_exit(ERROR_STATUS);
	}
	return process_execute(file);;
}

static int sys_wait(int pid) {
	return process_wait(pid);
}

static bool sys_create(const char *file, unsigned initial_size) {
	if (-1 == get_user((const uint8_t*) file)) {
		sys_exit(ERROR_STATUS);
	}
	lock_acquire(&file_lock);
	bool wasCreated = filesys_create(file, initial_size);
	lock_release(&file_lock);
	return wasCreated;
}

static bool sys_remove(const char *file) {
	if (-1 == get_user((const uint8_t*) file)) {
		sys_exit(ERROR_STATUS);
	}
	lock_acquire(&file_lock);
	bool wasRemoved = filesys_remove(file);
	lock_release(&file_lock);
	return wasRemoved;
}

static int sys_open(const char *file) {
	if (-1 == get_user((const uint8_t *) file)) {
		sys_exit(ERROR_STATUS);
	}

	lock_acquire(&file_lock);
	struct file *f = filesys_open(file);
	if (!f) {
		lock_release(&file_lock);
		return -1;
	}
	struct file_data *filed = malloc(sizeof(struct file_data));

	filed->file = f;
	filed->fd = thread_current()->fd;
	thread_current()->fd++;
	list_push_back(&thread_current()->file_list, &filed->elem);
	lock_release(&file_lock);

	return filed->fd;
}

static int sys_filesize(int fd) {
	lock_acquire(&file_lock);
	struct thread *t = thread_current();
	struct list_elem *e;
	struct file_data *filed;
	for (e = list_begin(&t->file_list); e != list_end(&t->file_list); e =
			list_next(e)) {
		filed = list_entry(e, struct file_data, elem);
		if (fd == filed->fd) {
			break;
		}
	}
	if (!filed->file) {
		lock_release(&file_lock);
		return -1;
	}
	int size = file_length(filed->file);
	lock_release(&file_lock);
	return size;
}

static int sys_read(int fd, void *buffer, unsigned size) {
	if (-1 == get_user((const uint8_t*) buffer)) {
		sys_exit(ERROR_STATUS);
	}
	lock_acquire(&file_lock);
	struct thread *t = thread_current();
	struct list_elem *e;
	struct file_data *filed;
	for (e = list_begin(&t->file_list); e != list_end(&t->file_list); e =
			list_next(e)) {
		filed = list_entry(e, struct file_data, elem);
		if (fd == filed->fd) {
			break;
		}
	}
	if (!filed->file) {
		lock_release(&file_lock);
		return -1;
	}
	int bytes_read = file_read(filed->file, buffer, size);
	lock_release(&file_lock);
	return bytes_read;
}

static int sys_write(int fd, const void *buffer, unsigned size) {
	if (-1 == get_user((const uint8_t*) buffer)) {
		sys_exit(ERROR_STATUS);
	}
	lock_acquire(&file_lock);
	struct thread *t = thread_current();
	struct list_elem *e;
	struct file_data *filed;
	for (e = list_begin(&t->file_list); e != list_end(&t->file_list); e =
			list_next(e)) {
		filed = list_entry(e, struct file_data, elem);
		if (fd == filed->fd) {
			break;
		}
	}
	if (!filed->file) {
		lock_release(&file_lock);
		return -1;
	}
	int bytes_written = file_write(filed->file, buffer, size);
	lock_release(&file_lock);
	return bytes_written;
}

static void sys_seek(int fd, unsigned position) {

	lock_acquire(&file_lock);
	struct thread *t = thread_current();
	struct list_elem *e;
	struct file_data *filed;
	for (e = list_begin(&t->file_list); e != list_end(&t->file_list); e =
			list_next(e)) {
		filed = list_entry(e, struct file_data, elem);
		if (fd == filed->fd) {
			break;
		}
	}
	if (!filed->file) {
		lock_release(&file_lock);
		return;
	}
	file_seek(filed->file, position);
	lock_release(&file_lock);
	return;
}

static unsigned sys_tell(int fd) {
	lock_acquire(&file_lock);
	struct thread *t = thread_current();
	struct list_elem *e;
	struct file_data *filed;
	for (e = list_begin(&t->file_list); e != list_end(&t->file_list); e =
			list_next(e)) {
		filed = list_entry(e, struct file_data, elem);
		if (fd == filed->fd) {
			break;
		}
	}
	if (!filed->file) {
		lock_release(&file_lock);
		return -1;
	}
	off_t position = file_tell(filed->file);
	lock_release(&file_lock);
	return position;
}

static void sys_close(int fd) {
	lock_acquire(&file_lock);
	struct thread *t = thread_current();
	struct list_elem *e;
	struct file_data *filed;

	//[SD]we have to iterate the list of files, close the file with the fd and remove it from list
	for (e = list_begin(&t->file_list); e != list_end(&t->file_list); e =
			list_next(e)) {
		filed = list_entry(e, struct file_data, elem);
		if (fd == filed->fd) {
			file_close(filed->file);
			list_remove(&filed->elem);
		}
	}
	lock_release(&file_lock);
}


/* Reads a byte at user virtual address UADDR.
 +   UADDR must be below PHYS_BASE.
 +   Returns the byte value if successful, -1 if a segfault
 +   occurred. */
static int get_user(const uint8_t *uaddr) {
	int result;
	asm ("movl $1f, %0; movzbl %1, %0; 1:"
			: "=&a" (result) : "m" (*uaddr));
	return result;
} //Pintos PDF provided code

/* Writes BYTE to user address UDST.
 UDST must be below PHYS_BASE.
 Returns true if successful, false if a segfault occurred. */
static bool put_user(uint8_t *udst, uint8_t byte) {
	int error_code;
	asm ("movl $1f, %0; movb %b2, %1; 1:"
			: "=&a" (error_code), "=m" (*udst) : "q" (byte));
	return error_code != -1;
} //Pintos PDF provided code
